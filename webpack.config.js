const path = require("path");
const HtmlWebPackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const appDir = path.resolve(__dirname, "app");
const publicDir = path.resolve(__dirname, "public");

const appHtml = path.join(appDir, path.join("html", "index.html"));

const htmlPlugin = new HtmlWebPackPlugin({
  template: appHtml
});

const extractTextPlugin = new ExtractTextPlugin({
  filename: '[name].[hash].css'
}); 

module.exports = {
  entry: appDir,
  output: {
    path: publicDir,
    filename: "[name].[hash].js"
  },
  resolve: {
    alias: {
      '../../theme.config$': path.join(__dirname, 'app/assets/styles/custom-semantic-theme/theme.config')
    }
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ["babel-loader"]
      },{
        test: /\.less$/,
        use: ExtractTextPlugin.extract({
          use: [
            'css-loader',
            'less-loader'
          ]
        })
      },{
        test: /\.css$/,
        use: [
          "style-loader",
          "css-loader"
        ]
      },{
        test: /\.jpe?g$|\.gif$|\.ico$|\.png$|\.svg$/,
        use: 'file-loader?name=[name].[ext]?[hash]'
      },{
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'url-loader?limit=10000&mimetype=application/font-woff'
      },{
        test: /\.(ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader'
      },{
        test: /\.otf(\?.*)?$/,
        use: 'file-loader?name=/fonts/[name].[ext]&mimetype=application/font-otf'
      }
    ]
  },
  plugins: [
    htmlPlugin,
    extractTextPlugin
  ],
  devtool: 'inline-source-map',
  devServer: {
    historyApiFallback: true
  }
};
