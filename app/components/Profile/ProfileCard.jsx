import React from 'react';
import PropTypes from 'prop-types';
import { Card, Icon, Image, Button } from 'semantic-ui-react';

const defaultPicture = require('./../../images/default.png');

const ProfileCard = ({ picture, name, work, about, totalFriends }) => (
    <Card fluid>
        <Card.Content>
            <Image
                floated='left'
                size='mini'
                src={defaultPicture}
            />
            <Card.Header>{name}</Card.Header>
            {work && <Card.Meta>{work}</Card.Meta>}
            {about && <Card.Description>{about}</Card.Description>}
        </Card.Content>
        <Card.Content extra>
            <Button.Group floated='left' size='small' basic>
                <Button>
                    <Icon name='smile' color='yellow'/>
                    3
                </Button>
                <Button>
                    <Icon name='like' color='red'/>
                    16
                </Button>
                <Button>
                    <Icon name='thumbs down' color='black'/>
                    1
                </Button>
            </Button.Group>
            <Button.Group floated='right'>
                <Button color='blue'>
                    <Icon name='like' />
                    Follow
                </Button>
            </Button.Group>
        </Card.Content>
    </Card>
);

ProfileCard.propTypes = {
    name: PropTypes.string.isRequired,
    work: PropTypes.string,
    about: PropTypes.string,
    totalFriends: PropTypes.number.isRequired
};

ProfileCard.defaultProps = {
    name: 'Unknown',
    totalFriends: 0
};

export default ProfileCard;