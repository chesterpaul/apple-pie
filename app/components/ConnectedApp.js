import { connect } from 'react-redux';

import { authenticate } from './../redux/actions/users';
import App from './App.jsx';

const mapStateToProps = ({ users: { isLogin } }) => ({
    isLogin
});

const mapStateToDispatchProps = dispatch => ({
    authenticate: (username, password) => dispatch(authenticate(username, password))
});

export default connect(mapStateToProps, mapStateToDispatchProps)(App);