import React, { Component } from 'react';

import {
  Switch,
  BrowserRouter as Router,
  Route
} from 'react-router-dom';

import { hot } from 'react-hot-loader';

import { Container, Card} from 'semantic-ui-react';

import "semantic-ui-less/semantic.less";
import "./../assets/styles/main.css";

import Menu from './Menu';
import Newsfeed from './Newsfeed';
import LoginPage from './Login';

class App extends Component {
  render() {
    const { isLogin } = this.props;

    if(!isLogin){
      return <LoginPage />
    }

    return (
      <Router>
        <div>
          <Menu />
          <Container text className="content-wrapper">
            <Switch>
              <Route exact path={"/"} component={Newsfeed} />
              <Route path={"/friends"} component={() => <h1>Friends</h1>} />
            </Switch>
          </Container>
        </div>
      </Router>
    );
  }
}

export default hot(module)(App);