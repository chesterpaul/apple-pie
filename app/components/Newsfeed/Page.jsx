import React from 'react';
import { Card } from 'semantic-ui-react';

import Post from './../Post'

import newsfeeds from './../../../__mocks__/newsfeed';

const Page = () => (
    <Card.Group>
        {
            newsfeeds.map((object, index) => <Post
            key={index}
            {...object}
            />)
        }
    </Card.Group>
);

export default Page;