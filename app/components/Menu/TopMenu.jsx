import React from 'react';
import PropTypes from 'prop-types';
import { Menu, Icon, Button, Dropdown, Label } from 'semantic-ui-react';

import { NavLink } from 'react-router-dom';

const Logo = require('./../../assets/images/logo.png');

const TopMenu = ({ activeItem, handleItemClick }) => (
    <Menu borderless fixed='top' size='small' color='yellow'>
        <Menu.Item header>
            <img src={Logo} />
        </Menu.Item>
        <Menu.Item
            exact
            as={NavLink}
            to={'/'}
            name='newsfeed'
            activeClassName="active"
        >
            <Icon name='newspaper' />
            Newsfeed
        </Menu.Item>
        <Menu.Item
            as={NavLink}
            to={'/friends'}
            name='friends'
            activeClassName="active"
        >
            <Icon name='users' />
            My Friends
        </Menu.Item>
        <Menu.Menu position='right'>
            <Dropdown item text='Welcome, Chester'>
                <Dropdown.Menu>
                    <Dropdown.Item>
                        Notifications&nbsp;
                        <Label circular color='red' size='tiny'>
                            22
                        </Label>
                    </Dropdown.Item>
                    <Dropdown.Item>Profile Settings</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
            <Menu.Item>
                <Button color='red'>Sign Out</Button>
            </Menu.Item>
        </Menu.Menu>
    </Menu>
);

TopMenu.propTypes = {
    activeItem: PropTypes.string,
    handleItemClick: PropTypes.func.isRequired
};

TopMenu.defaultProps = {
    handleItemClick: () => {}
};

export default TopMenu;