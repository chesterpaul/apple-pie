import { connect } from 'react-redux';

import { authenticate } from './../../redux/actions/users';

import Page from './Page.jsx';

const mapStateToProps = ({ users: { authenticating, errorMessage } }) => ({
    authenticating,
    errorMessage
});

const mapStateToDispatchProps = dispatch => ({
    authenticate: (username, password) => dispatch(authenticate(username, password))
});

export default connect(mapStateToProps, mapStateToDispatchProps)(Page);