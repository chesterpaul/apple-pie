import React, {Component} from 'react';
import { Button, Form, Grid, Header, Image, Segment, Message } from 'semantic-ui-react';

const Logo = require('./../../assets/images/logo.png');

class Page extends Component {
    state = {username: '', password: '', errorMessage: ''};

    onChange = (e, { name, value }) => this.setState({ [name]: value });

    validate = () => {
        const { username, password } = this.state;

        if(username === '' && password === '') {
            this.setState({ errorMessage: 'Username and Password are required' });
            return;
        }

        this.setState({ errorMessage: '' });
        this.props.authenticate(username, password);
    }

    render() {
        return (
            <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
                <Grid.Column style={{ maxWidth: 450 }}>
                <Header as='h2' color='teal' textAlign='center'>
                    <Image src={Logo} />Log-in to your account
                </Header>
                {
                    this.state.errorMessage && (
                        <Message negative>
                            <p>{this.state.errorMessage}</p>
                        </Message>
                    )
                }
                {
                    this.props.errorMessage && (
                        <Message negative>
                            <p>{this.props.errorMessage}</p>
                        </Message>
                    )
                }
                <Form size='large'>
                    <Segment stacked>
                    <Form.Input fluid icon='user' iconPosition='left' placeholder='Username' name='username' onChange={this.onChange}/>
                    <Form.Input
                        fluid
                        icon='lock'
                        iconPosition='left'
                        placeholder='Password'
                        type='password'
                        name='password'
                        onChange={this.onChange}
                    />

                    <Button color='teal' fluid size='large' onClick={this.validate} disabled={this.props.authenticating}>
                        Login
                    </Button>
                    </Segment>
                </Form>
                </Grid.Column>
            </Grid>
        )
    }
};

export default Page;