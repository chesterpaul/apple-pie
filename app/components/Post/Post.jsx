import React from 'react';
import PropTypes from 'prop-types';
import { Card, Icon, Image, Button, Grid, Divider, Label } from 'semantic-ui-react';
import Emoji from 'react-emoji-render';

const defaultPicture = require('./../../assets/images/default.png');

import currentUser from './../../../__mocks__/currentUser';

const buildPostPicture = (pictures, message) => {
    if(pictures.length === 0) return <Emoji text={message} />;
    
    return (
        <div>
            <Emoji text={message} />
            <Divider hidden/>
            <Grid>
                <Grid.Row columns={pictures.length}>
                    {pictures.map((src, key) => <Grid.Column key={key}><Image src={src} bordered/></Grid.Column>)}
                 </Grid.Row>
            </Grid>
        </div>
    );
};

const ifFriend = (currentUserFriends, userId) => currentUserFriends.indexOf(userId) !== -1;

const buildPostButton = (currentUserFriends, newsfeedId) => {
    if(ifFriend(currentUserFriends, newsfeedId)) return (
        <Button color='green'>
            <Icon name='send' />
            Message
        </Button>
    );

    return (
        <Button color='blue'>
            <Icon name='add user' />
            Follow
        </Button>
    );
};


const Post = ({ picture, name, postMessage, postPictures, likes, smiles, hearts, id }) => (
    <Card fluid color='olive'>
        <Card.Content>
            <Image
                floated='left'
                size='mini'
                src={picture}
                avatar
                bordered
            />
            <Card.Header>{name}&nbsp;{ ifFriend(currentUser.friends, id) && <Label as='a' tag size='tiny'>Followed</Label>}</Card.Header>
            <Card.Description>
                {
                    buildPostPicture(postPictures, postMessage)
                }
            </Card.Description>
        </Card.Content>
        <Card.Content extra>
            <Button.Group floated='left' size='small' basic>
                <Button>
                    <Icon name='thumbs up' color='black'/>{likes.length}
                </Button>
                <Button>
                    <Icon name='smile' color='yellow'/>{smiles.length}
                </Button>
                <Button>
                    <Icon name='like' color='red'/>{hearts.length}
                </Button>
            </Button.Group>
            <Button.Group floated='right'>
                {buildPostButton(currentUser.friends, id )}
            </Button.Group>
        </Card.Content>
    </Card>
);

Post.propTypes = {
    name: PropTypes.string.isRequired,
    postMessage: PropTypes.string.isRequired
};

Post.defaultProps = {
    picture: defaultPicture,
    postPictures: []
};

export default Post;