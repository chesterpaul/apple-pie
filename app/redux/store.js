import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';

import { isDev } from './../helpers/checker';
import reducers from './reducers';

const middlewares = [];

middlewares.push(ReduxThunk);

if(isDev()) {
    const { logger } = require('redux-logger');

    middlewares.push(logger);
}

export default createStore(reducers, {}, applyMiddleware(...middlewares));