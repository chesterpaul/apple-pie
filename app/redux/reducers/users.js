import { AUTHENTICATE_USER, AUTHENTICATE_USER_SUCCESS, AUTHENTICATE_USER_FAILED } from './action-types';

const initialState = {
    authenticating: false,
    errorMessage: '',
    isLogin: false,
    details: {}
};

const users = (state = initialState, action) => {
    switch(action.type) {
        case AUTHENTICATE_USER:
            return {
                ...state,
                authenticating: true
            };
        case AUTHENTICATE_USER_SUCCESS: {
            const { details } = action.payload;
            return {
                ...state,
                ...details,
                authenticating: false,
                isLogin: true
            };
        }
        case AUTHENTICATE_USER_FAILED:{
            const { errorMessage } = action.payload;
            return {
                ...state,
                errorMessage,
                authenticating: false
            };
        }
        default: return state;
    }
};

export default users;