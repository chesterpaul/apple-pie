import * as actionTypes from './../reducers/action-types';

import fakeApi from './../../../__mocks__/fakeApi';

export const authenticateAction = () => ({ type: actionTypes.AUTHENTICATE_USER });
export const setAuthSuccessAction = details => ({ type: actionTypes.AUTHENTICATE_USER_SUCCESS, payload: { details } });
export const setAuthFailedAction = errorMessage => ({ type: actionTypes.AUTHENTICATE_USER_FAILED, payload: { errorMessage } });

export const authenticate = (username, password) => {
    return dispatch => {
        dispatch(authenticateAction());
        fakeApi.auth(username, password).then(response => {
            dispatch(setAuthSuccessAction({ details: response.data }));
        }).catch(() => {
            dispatch(setAuthFailedAction("Invalid username or password"));
        })
    }
}