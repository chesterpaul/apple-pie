import credentials from './credentials';

export default {
    auth: (username, password) => new Promise((resolve, reject) => {
        setTimeout(() => {
            const existingUser = credentials.filter(row => row.password === password && row.username === username);
            console.log(username, password);
            console.log(existingUser);
            if(existingUser.length > 0){
                resolve({ status: true, data: existingUser[0] });
            }else{
                reject({ status: false })
            }
        }, 3000);
    })
}