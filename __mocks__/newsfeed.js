export default [
    {
        id: 1,
        name: "Rica Santos",
        postMessage: "Nice view! :D",
        postPictures: ['https://picsum.photos/id/90/500/500', 'https://picsum.photos/id/300/500/500', 'https://picsum.photos/id/77/500/500'],
        picture: 'https://i.pravatar.cc/150?u=1',
        likes: [2, 3],
        smiles: [2, 3, 4],
        hearts: [4, 5]
    },{
        id: 2,
        name: "John Dizon",
        postMessage: "GOD BLESS <3",
        picture: 'https://i.pravatar.cc/150?u=2',
        likes: [1],
        smiles: [2, 3],
        hearts: [1, 2, 3, 4, 5]
    },{
        id: 3,
        name: "Riza Perez",
        postMessage: "Photograpi! :D :D :D",
        picture: 'https://i.pravatar.cc/150?u=3',
        postPictures: ['https://picsum.photos/id/80/500/500', 'https://picsum.photos/id/89/500/500'],
        likes: [2, 3],
        smiles: [2, 3, 4],
        hearts: [4, 5]
    },{
        id: 4,
        name: "Jen Garcia",
        postMessage: "Goodluck ate for your entrance exam! :D :) <3",
        picture: 'https://i.pravatar.cc/150?u=4',
        likes: [2, 3],
        smiles: [2, 3, 4],
        hearts: [4, 5]
    },{
        id: 5,
        name: "John Mark",
        postMessage: "I love programming!",
        picture: 'https://i.pravatar.cc/150?u=5',
        likes: [2, 3],
        smiles: [2, 3, 4],
        hearts: [4, 5]
    },{
        id: 6,
        name: "Rico Paloma",
        postMessage: "Kudos everyone for making this event success! :)",
        picture: 'https://i.pravatar.cc/150?u=6',
        likes: [2, 3],
        smiles: [2, 3, 4],
        hearts: [4, 5]
    }
];